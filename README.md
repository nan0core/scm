# Source Code Management

## Problématique

Partager le code source d'une application devient nécessaire dès qu'on
travaille à plus qu'une personne sur un projet. Il arrive rapidemment des
problèmes quand on utilise des façons naïves de partager le code.

On veut résoudre quelques problèmes tel que:

- le partage du code facilement
- comprendre pourquoi un changement a été effectué
- la gestion des conflits d'édition
- la gestion de différentes versions du code
- le backport d'un bugfix d'une version vers l'autre
- existence d'outils pour Windows

### Le scénario

1. Alice et Bob partagent un ensemble de fichiers textes.
2. Alice fait un changement, le partage
3. Bob reçoit le changement d'Alice
4. Bob veut connaitre l'historique
5. Bob veut revenir en arrière
6. Alice et Bob éditent des fichiers différents.
7. Alice et Bob éditent le même fichier
8. On décide que le code est bon et un fait une release
9. On remarque un bug qu'on corrige et qu'on backporte


## Recherches

Mots clés pour nos recherches: *source code management*

-> Wikipédia nous donne quelques alternatives:

- mercurial
- git

### mercurial

http://hgbook.red-bean.com/

1. cp -a Alice-Bob Alice-Bob-hg
   cd Alice-Bob-hg
   hg init
   hg add File\*
   hg commit
   (on va chez Alice et Bob)
   hg clone $PWD/Alice-Bob-hg
2. (edit .hgrc pour mettre son nom)
   (édition)
   (hg diff pour voir le changement)
   hg commit
   hg push
3. hg pull
   hg update
4. hg log
   hg incoming
   hg annotate
   hg log --rev 1 --patch
5. hg update --rev 1
   hg update
6. hg push
   hg pull
   (hg heads)
   hg merge
   hg commit
   hg push
7. hg push
   hg pull
   (hg heads)
   hg resolve --list
   (edit)
   hg resolve --mark File1
   hg commit
8. Prépa release: Create CHANGELOG, changement version, etc
   hg commit
   hg tag 1.0.0
   hg branch 1.0
   hg commit
9. hg update default
   (add some changes)
   (fix the issue and commit the fix)
   hg up -r 1.0
   hg graft ...
   hg push

### git

https://git-scm.com/book/en/v2

1. git init --bare Alice-Bob.git
   cp -a Alice-Bob Alice-Bob-git
   cd Alice-Bob-git
   git init .
   git add File\*
   git commit
   git push --set-upstream ../Alice-Bob.git master
   (on va chez Alice et Bob)
   git clone $PWD/Alice-Bob.git
2. git config user.name Alice
   git config user.email alice@ipeps.example
   (édition)
   git add File1
   git commit
   git push
3. git pull (ce dernier fait les deux étapes)
4. git log
   git fetch
   git log ..origin/master
   git blame File1
   git log -p HASH~..HASH
5. git checkout HASH
   git checkout master
6. git push
   git pull
7. git push
   git pull
   git ls-files --unmerged
   (edit)
   git add File1
   git commit
   git push
8. Prépa release: Create CHANGELOG, changement version, etc
   git add
   git commit
   git tag -a 1.0.0
   git push --tags
9. git checkout master
   (fix the issue and commit the fix in master)
   git checkout 1.0.0
   git branch 1.0
   git cherry-pick ...
   git push --set-upstream origin 1.0
   git push --all

## Conclusions

### git

\+:
- Ultra répandu
- Rapide
- github, gitlab

\-:
- C'est quoi cette UI franchement ?
- L'UI est telle qu'on s'y perd et que parfois on perd son boulot


### mercurial

\+:
- UI plus compréhensible
- Jamais rien perdu

\-:
- Nettement moins répandu
- Pas de forge

ALEXANDRE
